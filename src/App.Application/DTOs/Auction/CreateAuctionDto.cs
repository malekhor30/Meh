using System;

namespace App.Application.DTOs.Auction
{
    public class CreateAuctionDto
    {
        public DateTime StartDate { get; set; }

        public uint ProductId { get; set; }
    }
}
