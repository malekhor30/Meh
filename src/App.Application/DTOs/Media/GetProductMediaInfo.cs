namespace App.Application.DTOs.Media;

public class GetProductMediaInfo
{
    public Guid? Id { get; set; }
    public string? Path { get; set; }
}
