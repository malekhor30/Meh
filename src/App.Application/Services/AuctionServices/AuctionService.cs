using App.Application.Contracts.Common;
using App.Application.DTOs.Auction;
using App.Application.DTOs.Common;
using App.Domain.Models.Auctions;
using App.Application.Extensions;
using App.Domain.Models.Auctions.AuctionParticipants;
using TanvirArjel.EFCore.GenericRepository;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace App.Application.Services.AuctionServices
{
    public class AuctionService
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;
        private readonly IDbService _dbService;
        private readonly IAuthenticationService _authService;

        public AuctionService(IAuthenticationService authService, IRepository repo, IDbService dbService, IMapper mapper)
        {
            _repo = repo;
            _dbService = dbService;
            _authService = authService;
            _mapper = mapper;
        }

        public async Task<Guid> CreateAuction(CreateAuctionDto dto, CancellationToken cancellationToken)
        {
            return await _dbService.CreateAsync<Auction, CreateAuctionDto, Guid>(dto, cancellationToken);
        }

        public Task DeleteAuction(Guid id, CancellationToken cancellationToken)
        {
            return _dbService.SoftDeleteAsync<Auction>(id, cancellationToken);
        }

        public async Task<GetAuctionInfoDto> GetAuctionInfo(Guid id, CancellationToken cancellationToken)
        {
            return await _dbService.GetByIdAsync<Auction, GetAuctionInfoDto>(id, cancellationToken);
        }

        public async Task<IEnumerable<GetAuctionInfoDto>> GetActiveAuctions(GetPageDto dto, CancellationToken cancellationToken)
        {
            return await _dbService.GetAsPageAsync<Auction, Guid, GetAuctionInfoDto, GetPageDto>(dto, auction => !auction.IsClosed && !auction.IsDeleted, cancellationToken);
        }

        public async Task<IEnumerable<GetAuctionInfoDto>> GetUserAuctions(GetPageDto dto, CancellationToken cancellationToken)
        {
            var userId = await _authService.GetCurrentUserId(cancellationToken);

            var auctionEntities = _repo.GetQueryable<AuctionParticipant>()
                                        .Include(row => row.Auction)
                                        .Where(row => row.ApplicationUserId == userId && !row.Auction.IsDeleted)
                                        .Select(row => row.Auction)
                                        .Skip((dto.PageId - 1) * dto.PageSize)
                                        .Take(dto.PageSize);

            var data = await _mapper.ProjectTo<GetAuctionInfoDto>(auctionEntities).ToListAsync(cancellationToken);

            return data;
        }
    }
}
