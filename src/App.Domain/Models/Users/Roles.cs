using System;

namespace App.Domain.Models.Users
{
    public class Roles
    {
        public const string ADMIN_ROLE = "ADMIN";
        public const string CUSTOMER_ROLE = "CUSTOMER";
        public const string SHOP_OWNER_ROLE = "SHOP_OWNER";
    }
}
