using App.Domain.Models.Auctions.AuctionOffers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infrastructure.DbContext.EntityTypeConigurations
{
    public class AuctionLogConfig : IEntityTypeConfiguration<AuctionOffer>
    {
        public void Configure(EntityTypeBuilder<AuctionOffer> builder)
        {
            builder.HasKey(auctionLog =>
            new
            {
                auctionLog.AuctionId,
                auctionLog.Date
            });
        }
    }
}
