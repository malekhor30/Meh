using App.Domain.Models.Products.Categories;
using Faker;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infrastructure.DbContext.EntityTypeConigurations
{
    public class PrductCategoryConfig : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            SeedCategoryTable(builder);
        }

        private static void SeedCategoryTable(EntityTypeBuilder<Category> builder)
        {
            var categories = Enumerable.Range(0, 6).Select(id =>
            {

                return Category.Factory(((short)(id + 1)), Name.First());
            });

            builder.HasData(categories);
        }
    }
}
