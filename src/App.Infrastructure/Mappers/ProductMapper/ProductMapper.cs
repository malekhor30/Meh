using System;
using App.Application.DTOs.Media;
using App.Application.DTOs.Product;
using App.Domain.Models.Products;
using App.Domain.Models.Products.Media;
using AutoMapper;

namespace App.Infrastructure.Mappers.ProductMapper
{
    public class ProductMapper : Profile
    {
        public ProductMapper()
        {
            CreateMap<CreateProductDto, Product>()
                    .ForMember(product => product.ShopOwnerId, opt => opt.Ignore());

            CreateMap<UpdateProductDto, Product>();

            CreateMap<Product, GetProductDto>()
                    .BeforeMap((product, dto) =>
                    {
                        dto.Company = new();
                        // dto.Category = new();
                    })
                    // .ForPath(dto => dto.Category.Id, opt => opt.MapFrom(product => product.CategoryId))
                    // .ForPath(dto => dto.Category.Name, opt => opt.MapFrom(product => product.Category.Name))
                    .ForPath(dto => dto.Company.CompanyId, opt => opt.MapFrom(product => product.ShopOwnerId))
                    .ForPath(dto => dto.Company.CompanyName, opt => opt.MapFrom(product => product.ShopOwner.NormalizedUserName));

            CreateMap<Product, OrderProductInfoDto>()
                    .ForMember(product => product.ProductId, opt => opt.MapFrom(dto => dto.Id));

            CreateMap<Media, GetProductMediaInfo>();
        }
    }
}
