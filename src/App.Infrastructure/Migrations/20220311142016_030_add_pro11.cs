﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace App.Infrastructure.Migrations
{
    public partial class _030_add_pro11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActivateConditions",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    Description = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivateConditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiscountTypes",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    Description = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    Cost = table.Column<double>(type: "double precision", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    OrderStatus = table.Column<int>(type: "integer", nullable: false),
                    ApplicationUserId = table.Column<string>(type: "text", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    Price = table.Column<double>(type: "double precision", nullable: false),
                    Quantity = table.Column<long>(type: "bigint", nullable: false),
                    Description = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    CategoryId = table.Column<short>(type: "smallint", nullable: false),
                    ShopOwnerId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_AspNetUsers_ShopOwnerId",
                        column: x => x.ShopOwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Auctions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    StartDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    IsClosed = table.Column<bool>(type: "boolean", nullable: false),
                    FinalPrice = table.Column<double>(type: "double precision", nullable: true),
                    InitialPrice = table.Column<double>(type: "double precision", nullable: false),
                    MinOfferPrice = table.Column<double>(type: "double precision", nullable: false),
                    CreatorId = table.Column<string>(type: "text", nullable: false),
                    WinnerId = table.Column<string>(type: "text", nullable: false),
                    ProductId = table.Column<long>(type: "bigint", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auctions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Auctions_AspNetUsers_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Auctions_AspNetUsers_WinnerId",
                        column: x => x.WinnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Auctions_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Discounts",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    EndDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Value = table.Column<int>(type: "integer", nullable: false),
                    DiscountTypeId = table.Column<short>(type: "smallint", nullable: false),
                    ProductId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Discounts_DiscountTypes_DiscountTypeId",
                        column: x => x.DiscountTypeId,
                        principalTable: "DiscountTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Discounts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Media",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    Path = table.Column<string>(type: "text", nullable: false),
                    IsPrimary = table.Column<bool>(type: "boolean", nullable: false),
                    ProductId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Media", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Media_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderProducts",
                columns: table => new
                {
                    ProductId = table.Column<long>(type: "bigint", nullable: false),
                    OrderId = table.Column<Guid>(type: "uuid", nullable: false),
                    Quantity = table.Column<long>(type: "bigint", nullable: false),
                    PricePerPiece = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProducts", x => new { x.OrderId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_OrderProducts_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuctionOffers",
                columns: table => new
                {
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    AuctionId = table.Column<Guid>(type: "uuid", nullable: false),
                    Price = table.Column<double>(type: "double precision", nullable: false),
                    ApplicationUserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuctionOffers", x => new { x.AuctionId, x.Date });
                    table.ForeignKey(
                        name: "FK_AuctionOffers_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuctionOffers_Auctions_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auctions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuctionParticipants",
                columns: table => new
                {
                    AuctionId = table.Column<Guid>(type: "uuid", nullable: false),
                    ApplicationUserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuctionParticipants", x => new { x.AuctionId, x.ApplicationUserId });
                    table.ForeignKey(
                        name: "FK_AuctionParticipants_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuctionParticipants_Auctions_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auctions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActivationInfo",
                columns: table => new
                {
                    MinValueToActivateDiscount = table.Column<int>(type: "integer", nullable: false),
                    DiscountId = table.Column<long>(type: "bigint", nullable: false),
                    ActivateConditionId = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "FK_ActivationInfo_ActivateConditions_ActivateConditionId",
                        column: x => x.ActivateConditionId,
                        principalTable: "ActivateConditions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivationInfo_Discounts_DiscountId",
                        column: x => x.DiscountId,
                        principalTable: "Discounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserDiscounts",
                columns: table => new
                {
                    DiscountId = table.Column<long>(type: "bigint", nullable: false),
                    ApplicationUserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDiscounts", x => new { x.ApplicationUserId, x.DiscountId });
                    table.ForeignKey(
                        name: "FK_UserDiscounts_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserDiscounts_Discounts_DiscountId",
                        column: x => x.DiscountId,
                        principalTable: "Discounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "330f3628-b644-45bb-96a1-4ae666e59126", "614bed0e-4688-4811-99f9-ab6ee87ab561", "ADMIN", "ADMIN" },
                    { "62a0d5a7-3518-410b-b3f0-96b76cbf0556", "f915af7f-77fc-4999-a632-46fcab68f1de", "SHOP_OWNER", "SHOP_OWNER" },
                    { "716d0263-8180-4312-8326-9d47bcc77696", "8a590cca-2db6-48a1-b13a-45aaf59bce59", "CUSTOMER", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "1bc18d1b-a2c7-4dbd-9edf-265264732d30", 0, "8a94b698-8c6a-4f6f-a651-403769bbb5bd", "customer@email.com", true, false, null, "CUSTOMER@EMAIL.COM", "CUSTOMER", "AQAAAAEAACcQAAAAED1njqf7cCD3xkwegKEzUYM3VGlHbKHCK6NjA3KWr0YhDc+wOMhHcu4knNqe+L0Nyw==", null, false, "0a3fb678-1287-4ff2-b613-587f0886e05c", false, "customer" },
                    { "52ea9045-163a-49d1-907b-33fa20da744c", 0, "5a9f778e-7c13-49a6-bee4-f2a03fd21cb9", "admin@email.com", true, false, null, "ADMIN@EMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEAMHG9xghtlETr08ico8Af1poe2/Q5M0BnXBfP85Q+RddpuUN+EUwWMUlMg29sdJWQ==", null, false, "033b9aca-3a57-4934-b52f-f79846a3b2b7", false, "admin" },
                    { "5a5719d3-d81d-4037-95a5-04ad09100d95", 0, "2994d2cf-c1a6-4ec6-969b-2629adf33502", "shopOwner@email.com", true, false, null, "SHOPOWNER@EMAIL.COM", "SHOPOWNER", "AQAAAAEAACcQAAAAEAT5s9MmJO/SYS1Ykuo7Oppskz+Us72K15VYpoLOIuI6xIju8l7lu7GFgKkYeuLabA==", null, false, "e800cbdf-20a7-4e20-81e4-e9fe77cd8d5c", false, "shopOwner" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { (short)1, "Jerrod" },
                    { (short)2, "Onie" },
                    { (short)3, "Ariane" },
                    { (short)4, "Fausto" },
                    { (short)5, "Arlie" },
                    { (short)6, "Christy" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "716d0263-8180-4312-8326-9d47bcc77696", "1bc18d1b-a2c7-4dbd-9edf-265264732d30" },
                    { "330f3628-b644-45bb-96a1-4ae666e59126", "52ea9045-163a-49d1-907b-33fa20da744c" },
                    { "62a0d5a7-3518-410b-b3f0-96b76cbf0556", "5a5719d3-d81d-4037-95a5-04ad09100d95" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Description", "Name", "Price", "Quantity", "ShopOwnerId" },
                values: new object[,]
                {
                    { 13474709L, (short)4, "Cupiditate dolorum beatae et qui quia commodi reprehenderit delectus voluptas.", "Humberto", 11.153, 346582508L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 22107695L, (short)1, "Omnis nisi ut possimus optio tenetur vel nobis.", "Jamarcus", 3.1960000000000002, 1188092934L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 25127179L, (short)3, "Voluptatem corporis nihil consequatur mollitia sint molestias.", "Aron", 6.0119999999999996, 213002084L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 47404510L, (short)2, "Impedit qui vero fugit architecto occaecati veniam voluptates voluptas.", "Meaghan", 9.3200000000000003, 620842832L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 48067289L, (short)1, "Illum atque quia minima reprehenderit non.", "Ronaldo", 10.372999999999999, 366807384L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 48070434L, (short)3, "Iste incidunt quisquam alias iusto est.", "Judah", 11.167999999999999, 401188237L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 48569832L, (short)1, "Quae explicabo ut ad dolorum ratione non sunt ratione dolores.", "Kobe", 4.7169999999999996, 1678039516L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 89352003L, (short)1, "Reprehenderit voluptatibus tenetur quia porro quod id odit vero.", "Domenic", 10.737, 1509266238L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 113906373L, (short)3, "Officia iure et deserunt sit magnam autem qui debitis.", "Horacio", 10.818, 930622698L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 118301607L, (short)4, "Consequatur assumenda non unde voluptatum delectus nobis.", "Israel", 11.554, 1887179829L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 120403817L, (short)1, "Aperiam sed voluptatibus quidem sed fuga odio occaecati dolor ut voluptas.", "Lucy", 0.14599999999999999, 1776473620L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 126671721L, (short)1, "Qui minima dolor consequatur et excepturi impedit.", "Clare", 9.1859999999999999, 102408352L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 130549435L, (short)2, "Sed saepe tempora qui ab ipsum placeat dolores.", "Arnoldo", 2.1859999999999999, 1780009484L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 186699060L, (short)2, "Quia ab voluptatem harum fuga sed animi et non.", "Cooper", 7.8280000000000003, 2096340096L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 259178713L, (short)1, "Odio vitae enim ipsa minima at libero non aut expedita nihil.", "Trisha", 10.042999999999999, 776698063L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 315178152L, (short)3, "Totam laborum tempore est ad cumque ipsum vero eos.", "Jaeden", 7.0129999999999999, 185123227L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 332637239L, (short)1, "Quis vero aut nihil nam.", "Joey", 7.3540000000000001, 500682543L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 345075814L, (short)2, "Nihil nihil vel perferendis aut porro in amet.", "Tiara", 9.1639999999999997, 1898287754L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 348993254L, (short)2, "In excepturi quibusdam ea molestiae.", "Rodrick", 12.545999999999999, 869642803L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 360037639L, (short)3, "Harum adipisci suscipit assumenda facilis.", "Garry", 0.32700000000000001, 442341150L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 376014240L, (short)2, "Provident non molestiae saepe similique magni enim quis recusandae rerum.", "Curt", 8.5760000000000005, 462964660L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 384104360L, (short)3, "Non et minima nisi inventore quo repellat excepturi repellendus qui.", "Michelle", 5.1029999999999998, 1106187832L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 395057456L, (short)2, "Dolores ut vel sit perspiciatis dolor quis voluptatem.", "Yasmin", 9.8130000000000006, 1101830714L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 412689663L, (short)1, "Consequuntur animi nihil id omnis.", "Allison", 0.53400000000000003, 173415323L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 416750258L, (short)3, "Non mollitia consequatur quos autem ex tempora.", "Rebeka", 9.2729999999999997, 2072355028L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 440760136L, (short)4, "Atque sint esse rerum adipisci dolor incidunt dolores.", "Cora", 9.2940000000000005, 980187094L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 470708799L, (short)1, "Aut dolorem nihil est ad voluptate et.", "Gavin", 1.2470000000000001, 2036096295L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 476020107L, (short)2, "Suscipit aliquam exercitationem quaerat est eum sed rem voluptatem laborum voluptatem.", "Tyrell", 8.8490000000000002, 649616353L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 506384873L, (short)4, "Tempora saepe recusandae vel iusto incidunt impedit nemo cupiditate qui.", "Mariano", 10.148999999999999, 484054847L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 524080397L, (short)1, "Debitis culpa at fugiat dolore alias facilis.", "Jackie", 0.014999999999999999, 373957680L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 528531482L, (short)3, "Officiis nostrum quod natus illum impedit consequatur deserunt autem libero.", "General", 6.7270000000000003, 842212912L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 534852698L, (short)1, "Ut voluptatum aperiam praesentium et reprehenderit exercitationem mollitia reprehenderit vel.", "Nina", 11.202, 1573789271L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 549331882L, (short)1, "Suscipit mollitia voluptas saepe sit.", "Otilia", 10.584, 205372946L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 561058543L, (short)3, "Aliquid fuga incidunt debitis ab neque quia ipsum.", "Bernice", 2.5960000000000001, 1432091358L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 569896462L, (short)1, "Ea quibusdam beatae ut sunt nihil culpa distinctio aut.", "Erica", 3.4359999999999999, 256058424L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 571973292L, (short)2, "Asperiores omnis error quia fugit officiis et quidem consequatur dolor.", "Deontae", 8.3889999999999993, 1890568929L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 606368641L, (short)4, "Quia possimus quas nulla ullam repellat provident.", "Easter", 10.029999999999999, 255853414L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 615118358L, (short)1, "Eaque reiciendis earum inventore aperiam fugit ut veritatis.", "Joanny", 2.5270000000000001, 487942369L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 644592525L, (short)2, "Nihil voluptate sed voluptas magnam magnam et consequuntur vero dolores fuga.", "Cale", 10.497999999999999, 2044261118L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 653055924L, (short)2, "Fugiat aut deleniti reiciendis animi et earum itaque aut non.", "Corine", 4.6189999999999998, 1488356434L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 658488112L, (short)4, "Ex ipsa tenetur commodi illo sunt qui pariatur corporis esse.", "Elise", 8.1170000000000009, 63510086L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 687778915L, (short)2, "Non cupiditate illum quaerat placeat harum.", "Bonita", 5.0380000000000003, 1584162479L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 700851735L, (short)2, "Modi nihil fuga rerum error nobis.", "Ellsworth", 8.8209999999999997, 426790365L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 727401777L, (short)1, "Et voluptas voluptatum velit odio sunt similique voluptas minima.", "Heath", 11.244999999999999, 974391899L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 746491168L, (short)2, "Ex debitis soluta ad reprehenderit nemo dolorem autem facilis.", "Maggie", 12.616, 642256682L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 749489236L, (short)3, "Atque maiores nisi necessitatibus necessitatibus.", "Nicolette", 4.1369999999999996, 1346088111L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 772052859L, (short)3, "Animi officiis ex sit voluptatem quia.", "Melyna", 8.048, 1927826673L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 777198745L, (short)3, "Veniam esse maxime iure magnam odio eos beatae totam.", "Jameson", 11.079000000000001, 1150533739L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 778122430L, (short)4, "Cum aut magni ipsa voluptatem voluptates.", "Kiel", 1.7729999999999999, 1155879274L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 784063439L, (short)3, "Quae delectus dicta architecto et sunt cumque ipsa.", "Margret", 7.3710000000000004, 578099433L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 787489109L, (short)3, "Excepturi eos aut odit sapiente nam.", "Edd", 7.2800000000000002, 1556059810L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 805026060L, (short)3, "Molestiae est error officiis molestiae.", "Clint", 3.1299999999999999, 1949002360L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 820197367L, (short)2, "Facere sed magni quia tempora commodi quibusdam qui necessitatibus sit qui.", "Murphy", 12.289, 194339137L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 841230804L, (short)4, "Sunt odio culpa voluptates similique numquam.", "Hillard", 6.1050000000000004, 1513309438L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 853109228L, (short)2, "Dolorem perspiciatis nemo quod quae asperiores.", "Ramon", 2.7890000000000001, 2046922692L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 864115716L, (short)4, "Dolorem ipsam debitis veniam ducimus et aliquid et in rerum.", "Gunner", 3.9820000000000002, 1327910240L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 870460016L, (short)4, "Ullam earum vero qui voluptatem rem mollitia.", "Rebeca", 6.8959999999999999, 1723429179L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 876532534L, (short)3, "Voluptatum veniam eveniet ducimus quo cum quis neque.", "Leora", 5.9640000000000004, 1707207434L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 885196361L, (short)2, "Iusto minima quas saepe et et dolorem at ipsam officia.", "Kaela", 11.612, 259290214L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 889285850L, (short)4, "Sint autem omnis vel odio blanditiis quam et nostrum.", "Justice", 5.7450000000000001, 2129504060L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 892964504L, (short)3, "Velit quia qui enim eveniet aut.", "Edison", 11.888, 393823396L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 896121276L, (short)4, "Ut cupiditate velit molestiae cupiditate rerum itaque quis ipsam necessitatibus deserunt.", "Maddison", 10.742000000000001, 2021851819L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 908802919L, (short)3, "Amet molestias sequi cupiditate voluptates perferendis ad quae animi optio.", "Leonardo", 1.2929999999999999, 2019744087L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 923686443L, (short)3, "Maiores eum enim odio vitae esse totam et aliquid.", "Steve", 9.7880000000000003, 1550316827L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 947328264L, (short)1, "Eaque ut expedita ad a sed.", "Maurine", 7.407, 1506951753L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 988570812L, (short)2, "Laborum ab dolores aliquid cupiditate autem numquam expedita aspernatur dolorem aut.", "Oswald", 9.7140000000000004, 31973421L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 994576606L, (short)2, "Assumenda qui voluptatem optio temporibus quo ut sit at.", "Desiree", 0.97099999999999997, 783462036L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1003941048L, (short)2, "Est cum nostrum provident unde tempore vero veritatis corporis qui.", "Florida", 0.77800000000000002, 1087685315L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1034936711L, (short)1, "Maxime expedita consequatur ab omnis perspiciatis esse aut vitae similique.", "Mustafa", 6.367, 1168217913L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1051089671L, (short)1, "Possimus iure nobis voluptas fuga similique nulla sit quis.", "Kylie", 3.1299999999999999, 1053864981L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1059293135L, (short)4, "Illo dignissimos corporis eligendi exercitationem nemo eligendi harum sapiente qui.", "Davin", 0.77200000000000002, 813444170L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1061742990L, (short)2, "Nemo autem odio numquam amet velit maxime.", "Lester", 9.6180000000000003, 1826446956L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1072169606L, (short)2, "Labore dolores dignissimos et aliquid facilis.", "Josh", 7.9729999999999999, 1952937444L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1094354678L, (short)2, "At expedita sint laudantium quia labore culpa.", "Bradford", 6.633, 1575460012L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1112860921L, (short)2, "Molestias vero quidem molestiae molestias consequatur ea eum deserunt aliquid.", "Allan", 6.6139999999999999, 2097762935L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1116750451L, (short)1, "Incidunt hic sunt eveniet sit itaque vero aut similique ipsam.", "Adriel", 9.7210000000000001, 697625806L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1150915976L, (short)2, "Non architecto iure autem dolores consectetur praesentium.", "Quentin", 5.7030000000000003, 950762862L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1157908059L, (short)3, "Deserunt et sit reiciendis sit consectetur et architecto in numquam.", "Daryl", 4.3280000000000003, 27170258L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1161476323L, (short)3, "Culpa a quaerat sed voluptates laudantium nobis ex.", "Reece", 8.4689999999999994, 1929137445L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1217079306L, (short)2, "At veritatis nihil odit sunt quod.", "Naomi", 1.633, 810134750L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1242665038L, (short)1, "Aut cupiditate cum reprehenderit sit fuga.", "Kelsie", 1.716, 569824502L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1270476279L, (short)1, "Non ut ratione odio perferendis.", "Jefferey", 2.2669999999999999, 1628528623L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1274112844L, (short)3, "Impedit ut est et ut aut eum nobis non.", "Stanton", 2.3039999999999998, 1088015062L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1292547601L, (short)2, "Molestiae ea voluptas omnis non et iste recusandae repellendus.", "Donna", 0.80100000000000005, 1833381179L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1312947830L, (short)2, "Inventore praesentium itaque autem itaque sit quidem impedit.", "Norma", 7.7530000000000001, 1584842815L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1319865050L, (short)4, "Voluptates fugit occaecati ea occaecati rerum sed error.", "Nyah", 2.5110000000000001, 1032258903L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1329834900L, (short)4, "Ut ducimus ex saepe sapiente culpa.", "Elroy", 6.2460000000000004, 488185148L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1341438337L, (short)1, "Non maxime laudantium ut nesciunt quaerat quos et eius voluptate consequuntur.", "Lemuel", 11.016999999999999, 1035368567L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1348688844L, (short)1, "In est dolor est nulla.", "Stewart", 4.9809999999999999, 710133558L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1356508031L, (short)2, "In at occaecati perspiciatis rerum eos dolore placeat id aut.", "Marcus", 1.9850000000000001, 1973322161L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1392193941L, (short)3, "Quidem aut autem et quas harum quia necessitatibus dolore quam iste.", "Lesly", 6.1230000000000002, 1855531803L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1395470862L, (short)2, "Aut cumque sunt animi doloribus quidem id cumque mollitia dolorum.", "Jamir", 5.0570000000000004, 1394623808L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1401299178L, (short)1, "Sunt et perferendis accusantium blanditiis distinctio ab qui non dignissimos nemo.", "Rollin", 1.7869999999999999, 1427274056L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1424803877L, (short)4, "Iste et enim nulla repudiandae.", "Granville", 6.4459999999999997, 949547362L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1434314301L, (short)4, "Iusto commodi in delectus sint.", "Carmelo", 2.883, 296208581L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1448209687L, (short)2, "Eos non quas deleniti velit dolorem dicta consequuntur aut at.", "Gay", 9.4459999999999997, 1098500510L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1461720217L, (short)3, "Rerum aperiam sit quaerat et cum consequatur et quaerat.", "Xander", 8.9030000000000005, 755488659L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1471426787L, (short)1, "Amet harum voluptatem qui perferendis doloremque sunt voluptatem.", "Coleman", 4.8769999999999998, 975152505L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1477335415L, (short)3, "Maxime consequatur officia est voluptatibus error nobis.", "Tyreek", 5.4900000000000002, 2099599309L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1488426294L, (short)4, "Consequuntur magnam aliquam veritatis id fugiat quaerat quo rem voluptatibus.", "Michele", 0.80900000000000005, 611573496L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1517087887L, (short)3, "Doloribus enim eligendi eaque excepturi ducimus ex iste blanditiis ea sint.", "Elyssa", 11.826000000000001, 994715434L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1522055122L, (short)1, "Officiis cupiditate consectetur sapiente exercitationem quia.", "Haleigh", 11.81, 1775569322L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1523450954L, (short)1, "Laborum quasi eum laboriosam ad facere ut aut.", "Garrison", 3.7090000000000001, 156647072L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1529304869L, (short)2, "Omnis voluptate rerum necessitatibus et perferendis.", "Yvonne", 6.7590000000000003, 955022999L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1590776676L, (short)3, "Quisquam dolore aperiam excepturi est illum.", "Riley", 3.1259999999999999, 2060898456L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1602047484L, (short)4, "Quibusdam non non voluptas quasi omnis nostrum sed aut quia.", "Hal", 4.4560000000000004, 1311668221L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1603664972L, (short)3, "Illo consequatur ea ducimus quas voluptatem saepe omnis et illum.", "Buster", 12.884, 1992202191L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1606110362L, (short)2, "Quia ea animi aut mollitia.", "Aimee", 11.706, 1372238341L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1607301227L, (short)2, "Est et alias est eos molestiae eos itaque voluptatum.", "Myrna", 1.7110000000000001, 1727753047L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1608492427L, (short)1, "Quasi adipisci quaerat voluptatem eveniet.", "Hope", 1.0700000000000001, 55359984L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1620070090L, (short)1, "Nulla laudantium eius sed et.", "Else", 5.8019999999999996, 1582394664L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1631399312L, (short)1, "Iusto fugiat omnis ratione eum a quibusdam.", "Dasia", 5.3449999999999998, 54797029L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1682633925L, (short)2, "Necessitatibus velit nisi iure molestiae ut natus temporibus.", "Kip", 2.395, 750138136L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1698052413L, (short)3, "Quia modi tempora sit doloribus voluptate.", "Roel", 9.8300000000000001, 1737346901L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1726333977L, (short)3, "Enim fugit eos accusantium incidunt earum velit debitis expedita laborum nihil.", "Sonia", 5.8170000000000002, 428308608L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1749794133L, (short)2, "Debitis voluptatem ea rerum id blanditiis rerum.", "Lauriane", 5.1680000000000001, 968590011L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1772842340L, (short)2, "Consequuntur a porro repudiandae voluptas distinctio quas delectus.", "Mason", 12.382999999999999, 651002659L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1777691132L, (short)1, "Repellendus ut et animi ut.", "Alana", 5.3719999999999999, 1528854118L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1831327125L, (short)4, "Voluptas sit inventore dolore nam occaecati.", "Keeley", 5.3559999999999999, 2102626986L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1852345351L, (short)4, "In tempora qui sunt officiis vero ut.", "Christophe", 3.1619999999999999, 1226288383L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1870449610L, (short)3, "Dicta corrupti officia voluptate optio voluptatem aperiam.", "Ivy", 2.831, 415550550L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1885144525L, (short)4, "Non sed eaque id inventore quis a totam.", "Camryn", 11.521000000000001, 751848751L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1906585364L, (short)3, "Doloremque voluptas quidem dicta impedit blanditiis eveniet qui.", "Edgardo", 9.9760000000000009, 264078696L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1908640140L, (short)1, "Non voluptatem ab et aspernatur repellat assumenda id eos.", "Bernhard", 4.431, 1156734715L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1925486977L, (short)1, "Et ea quia in incidunt maxime aut incidunt vitae.", "Alvera", 1.6930000000000001, 1246507076L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1933198330L, (short)2, "Maxime non inventore ut reiciendis et architecto.", "Royal", 12.093999999999999, 1126899452L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1943518146L, (short)4, "Illo doloremque et iure deleniti rem sequi placeat dolor maiores.", "Eloisa", 9.4130000000000003, 122224436L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1995394242L, (short)2, "Soluta tempore itaque occaecati a vel.", "Agustin", 6.2160000000000002, 2061708409L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 1998722887L, (short)1, "Minus molestiae quia tenetur eum quam voluptatum itaque labore.", "Donna", 2.4860000000000002, 202498901L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 2040073141L, (short)1, "Eum facere quibusdam cum placeat.", "Malinda", 2.3450000000000002, 2140651949L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 2053479778L, (short)3, "Voluptates autem perferendis et voluptatem voluptas dolorem non laudantium expedita earum.", "Ralph", 2.8730000000000002, 1560982036L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 2082028417L, (short)2, "Accusantium nihil corrupti ab perferendis labore incidunt tenetur omnis.", "Vesta", 12.010999999999999, 1768720697L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 2091830662L, (short)3, "Ipsam voluptatem ea reprehenderit illum.", "Anastacio", 4.1500000000000004, 1241015785L, "5a5719d3-d81d-4037-95a5-04ad09100d95" },
                    { 2146011768L, (short)4, "Nobis et occaecati non sequi architecto aut velit sunt.", "Greta", 2.472, 1728155465L, "5a5719d3-d81d-4037-95a5-04ad09100d95" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivationInfo_ActivateConditionId",
                table: "ActivationInfo",
                column: "ActivateConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivationInfo_DiscountId",
                table: "ActivationInfo",
                column: "DiscountId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuctionOffers_ApplicationUserId",
                table: "AuctionOffers",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuctionParticipants_ApplicationUserId",
                table: "AuctionParticipants",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Auctions_CreatorId",
                table: "Auctions",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Auctions_ProductId",
                table: "Auctions",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Auctions_WinnerId",
                table: "Auctions",
                column: "WinnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Discounts_DiscountTypeId",
                table: "Discounts",
                column: "DiscountTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Discounts_ProductId",
                table: "Discounts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Media_ProductId",
                table: "Media",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_ProductId",
                table: "OrderProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ApplicationUserId",
                table: "Orders",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShopOwnerId",
                table: "Products",
                column: "ShopOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDiscounts_DiscountId",
                table: "UserDiscounts",
                column: "DiscountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivationInfo");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "AuctionOffers");

            migrationBuilder.DropTable(
                name: "AuctionParticipants");

            migrationBuilder.DropTable(
                name: "Media");

            migrationBuilder.DropTable(
                name: "OrderProducts");

            migrationBuilder.DropTable(
                name: "UserDiscounts");

            migrationBuilder.DropTable(
                name: "ActivateConditions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Auctions");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Discounts");

            migrationBuilder.DropTable(
                name: "DiscountTypes");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
