using App.Application.DTOs.Common;
using App.Domain.Exceptions.LogicalExceptions;
using App.Domain.Exceptions.LogicalExceptions.CommonExceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CustomResponse = App.Application.DTOs.Common.Response;

namespace App.Presentation.Controllers.Common
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public abstract class BaseController<TService>
    {
        protected readonly TService _service;
        protected BaseController(TService service)
        {
            _service = service;
        }

        protected async Task<Response> NoContent(Func<CancellationToken, Task> task, CancellationToken cancellationToken)
        {
            try
            {
                await task(cancellationToken);

                return  Response.NoContent();
            }
            catch(AuthenticationException)
            {
                return Response.Unauthorized();
            }
            catch(AuthorizationException ex)
            {
                return Response.Forbidden(ex.Message);
            }
            catch(NotFoundException ex)
            {
                return Response.NotFound(ex.Message);
            }
            catch(BadRequestException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch(LogicalException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return Response.Error();
            }
        }

        protected async Task<Response> NoContent<TParam>(Func<TParam, CancellationToken, Task> task, TParam param, CancellationToken cancellationToken)
        {
            try
            {
                await task(param, cancellationToken);

                return Response.NoContent();
            }
            catch(AuthenticationException)
            {
                return Response.Unauthorized();
            }
            catch(AuthorizationException ex)
            {
                return Response.Forbidden(ex.Message);
            }
            catch(NotFoundException ex)
            {
                return Response.NotFound(ex.Message);
            }
            catch(BadRequestException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch(LogicalException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return Response.Error();
            }
        }

        protected async Task<Response> Ok<TParam, TResult>(Func<TParam, CancellationToken, Task<TResult>> task, TParam param, CancellationToken cancellationToken)
        {
            try
            {
                var data = await task(param, cancellationToken);

                return Response.Ok(data);
            }
            catch(AuthenticationException)
            {
                return Response.Unauthorized();
            }
            catch(AuthorizationException ex)
            {
                return Response.Forbidden(ex.Message);
            }
            catch(NotFoundException ex)
            {
                return Response.NotFound(ex.Message);
            }
            catch(BadRequestException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch(LogicalException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                if(ex.InnerException is not null) message += ex.InnerException.Message;

                return Response.Error(message);
            }
        }

        protected async Task<Response> Ok<TParam1, TParam2, TParam3, TResult>(Func<TParam1, TParam2, TParam3, CancellationToken, Task<TResult>> task, TParam1 param1, TParam2 param2, TParam3 param3, CancellationToken cancellationToken)
        {
            try
            {
                var data = await task(param1, param2, param3, cancellationToken);

                return Response.Ok(data);
            }
            catch(AuthenticationException)
            {
                return Response.Unauthorized();
            }
            catch(AuthorizationException ex)
            {
                return Response.Forbidden(ex.Message);
            }
            catch(NotFoundException ex)
            {
                return Response.NotFound(ex.Message);
            }
            catch(BadRequestException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch(LogicalException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                if(ex.InnerException is not null) message += ex.InnerException.Message;

                return Response.Error(message);
            }
        }

        protected async Task<Response> Ok<TResult>(Func<CancellationToken, Task<TResult>> task, CancellationToken cancellationToken)
        {
            try
            {
                var data = await task(cancellationToken);

                return Response.Ok(data);
            }
            catch(AuthenticationException)
            {
                return Response.Unauthorized();
            }
            catch(AuthorizationException ex)
            {
                return Response.Forbidden(ex.Message);
            }
            catch(NotFoundException ex)
            {
                return Response.NotFound(ex.Message);
            }
            catch(BadRequestException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch(LogicalException ex)
            {
                return Response.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Response.Error(ex.Message);
            }
        }
    }
}
