using App.Application.Contracts;
using App.Application.DTOs.Photo;
using App.Presentation.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

namespace App.Presentation.Controllers;

public class MediaController : BaseController<IProductMediaService>
{
    public MediaController(IProductMediaService service) : base(service)
    {
    }

    [HttpPost]
    public async Task<Guid> AddProductIcon([FromRoute] uint productId, [FromForm] IFormFile file, CancellationToken cancellationToken)
    {
        return await _service.AddProductIcon(productId, file, cancellationToken);
    }

    [HttpPost]
    public async Task AddProductMedia([FromRoute] uint productId, [FromForm] IEnumerable<IFormFile> files, CancellationToken cancellationToken)
    {
        await _service.AddProductMedia(productId, files, cancellationToken);
    }

    [HttpDelete]
    public async Task RemoveMedia(Guid id, CancellationToken cancellationToken)
    {
        await _service.RemoveMedia(id, cancellationToken);
    }

    [HttpPatch]
    public async Task MarkMediaAsPrimary(Guid id, CancellationToken cancellationToken)
    {
        await _service.MarkMediaAsPrimary(id, cancellationToken);
    }
}
